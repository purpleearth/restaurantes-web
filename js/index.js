$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');

        $('#btncontacto').removeClass('btn-outline-success');
        $('#btncontacto').addClass('btn-primary');
        $('#btncontacto').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('el modal se mostro');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('el modal se esta ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('el modal se oculto');

        $('#btncontacto').removeClass('btn-primary');
        $('#btncontacto').addClass('btn-outline-success');
        $('#btncontacto').prop('disabled', false);
    });
});